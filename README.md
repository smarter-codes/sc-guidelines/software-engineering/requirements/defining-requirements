# Defining/Expanding Requirements

Requirements offered by Product Design team need to be 'expanded' further before Engineering could be done upon it. 
Once you have [Understood requirements from Product Design team](https://gitlab.com/smarter-codes/guidelines/software-engineering/requirements/understand-product-design) we would expand (or define) the requirements further by these steps
* [Non functional requirements](https://en.wikipedia.org/wiki/Non-functional_requirement)
* Process Flow. [See example](https://drive.google.com/file/d/1ADDw0Peuw25wR8hLwamQYqbyTS4Hkku5/view?usp=sharing)
* Use Cases / User Stories. [See example](https://docs.google.com/document/d/1hIMtQdWNJ7nuFabzCFd2JexvYbl0FSXsHo17-kadFIo/edit?usp=sharing)
* [How Process Flow diagram is integrated with Use Cases](https://www.loom.com/share/4f396c49e88d4ea0a88808326db405f4)

# Inspirations
* [Metrices for Apps](https://web.dev/metrics/)
* [How to build a Performance Culture at Scale](https://medium.com/farfetch-tech-blog/how-to-build-a-performance-culture-at-scale-1ed8dc8e79cd)
* [Software Metrics](https://en.wikipedia.org/wiki/Category:Software_metrics)
* [Software Performance Testing](https://en.wikipedia.org/wiki/Software_performance_testing)
